import React from "react";
import { Route } from "react-router-dom";
import MainPageContainer from "../MainPageContainer";
import AuthPageContainer from "../AuthPageContainer";
import RegPageContainer from "../RegPageContainer";
import PageDawnstarContainer from "../Pages/PageDawnstarContainer";
import PageFolkrithContainer from "../Pages/PageFolkrithContainer";
import PageMarcartContainer from "../Pages/PageMarcartContainer";
import PageMorfhalContainer from "../Pages/PageMorfhalContainer";
import PageWhiterunContainer from "../Pages/PageWhiterunContainer";
import PageRiftenContainer from "../Pages/PageRiftenContainer";
import PageSolitudeContainer from "../Pages/PageSolitudeContainer";
import PageWindhelmContainer from "../Pages/PageWindhelmContainer";
import PageWinterholdContainer from "../Pages/PageWinterholdContainer";

function App() {
  return (
    <>
      <Route path="/" exact component={MainPageContainer} />
      <Route path="/auth" exact component={AuthPageContainer} />
      <Route path="/reg" exact component={RegPageContainer} />
      <Route path="/dawnstar" exact component={PageDawnstarContainer} />
      <Route path="/folkrith" exact component={PageFolkrithContainer} />
      <Route path="/marcart" exact component={PageMarcartContainer} />
      <Route path="/morfhal" exact component={PageMorfhalContainer} />
      <Route path="/whiterun" exact component={PageWhiterunContainer} />
      <Route path="/riften" exact component={PageRiftenContainer} />
      <Route path="/solitude" exact component={PageSolitudeContainer} />
      <Route path="/windhelm" exact component={PageWindhelmContainer} />
      <Route path="/winterhold" exact component={PageWinterholdContainer} />
      
    </>
  );
}

export default App;
