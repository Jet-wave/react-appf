import React, { FC } from "react";
import PageMorfhal from "../../../Components/Pages/PageMorfhal";

const PageMorfhalContainer: FC = () => {
  return (
    <>
      <PageMorfhal />
    </>
  );
};

export default PageMorfhalContainer;
