import React, { FC } from "react";
import PageDawnstar from "../../../Components/Pages/PageDawnstar";

const PageDawnstarContainer: FC = () => {
  return (
    <>
      <PageDawnstar />
    </>
  );
};

export default PageDawnstarContainer;
