import React, { FC } from "react";
import PageWindhelm from "../../../Components/Pages/PageWindhelm";

const PageWindhelmContainer: FC = () => {
  return (
    <>
      <PageWindhelm />
    </>
  );
};

export default PageWindhelmContainer;
