import React, { FC } from "react";
import PageMarcart from "../../../Components/Pages/PageMarcart";

const PageMarcartContainer: FC = () => {
  return (
    <>
      <PageMarcart />
    </>
  );
};

export default PageMarcartContainer;
