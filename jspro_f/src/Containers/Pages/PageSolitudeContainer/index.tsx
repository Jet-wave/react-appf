import React, { FC } from "react";
import PageSolitude from "../../../Components/Pages/PageSolitude";

const PageSolitudeContainer: FC = () => {
  return (
    <>
      <PageSolitude />
    </>
  );
};

export default PageSolitudeContainer;
