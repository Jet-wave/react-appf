import React, { FC } from "react";
import PageWhiterun from "../../../Components/Pages/PageWhiterun";

const PageWhiterunContainer: FC = () => {
  return (
    <>
      <PageWhiterun />
    </>
  );
};

export default PageWhiterunContainer;
