import React, { FC } from "react";
import PageFolkrith from "../../../Components/Pages/PageFolkrith";

const PageFolkrithContainer: FC = () => {
  return (
    <>
      <PageFolkrith />
    </>
  );
};

export default PageFolkrithContainer;
