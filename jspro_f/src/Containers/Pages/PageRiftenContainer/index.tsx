import React, { FC } from "react";
import PageRiften from "../../../Components/Pages/PageRiften";

const PageRiftenContainer: FC = () => {
  return (
    <>
      <PageRiften />
    </>
  );
};

export default PageRiftenContainer;
