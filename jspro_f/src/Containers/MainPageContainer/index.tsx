import React, { FC } from "react";
import MainPage from "../../Components/MainPage";

const MainPageContainer: FC = () =>{
  return (
    <>
      <MainPage />
    </>
  );
};

export default MainPageContainer;
