export enum Actions {
setUserLogin = 'SET_USER_LOGIN',
setUserPassword = 'SET_USER_PASSWORD',
}


export const setUserLoginAction = (login:string)=>{
    return{
        type: Actions.setUserLogin,
        payload: login,
    };
};

export const setUserPasswordAction = (password:string)=>{
    return{
        type: Actions.setUserPassword,
        payload: password,
    };
};