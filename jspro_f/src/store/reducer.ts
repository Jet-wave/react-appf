import { TReducer } from "./types";
import { Actions } from "./actions";



const initialState: TReducer ={
    login: "",
    password: "",
}

const reducer =(state = initialState, action: {
    type: string;
    payload?: any;
})=>{
    switch(action.type) {
        case Actions.setUserLogin:
            return {
                ...state,
                login: action.payload,
            };
            case Actions.setUserPassword:
                return {
                    ...state,
                    password: action.payload,
                };
            default: 
            return state;
    }
};

export default reducer;