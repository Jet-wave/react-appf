import React, { FC, useState } from "react";
import { useDispatch } from "react-redux";
import {
  setUserLoginAction,
  setUserPasswordAction,
} from "../../../store/actions";
import TextInput from "../../Common/Forms/TextInput";
import style from "./AuthForm.module.scss";

const AuthForm: FC = () => {
  const [login, setLogin] = useState<string>("");
  const [name, setName] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const dispatch = useDispatch();

  const passwordInputHadler = (event: any) => {
    setPassword(event.currentTarget.value);
  };

  const onSubmit = () => {
    dispatch(setUserLoginAction(login));
    dispatch(setUserPasswordAction(password));
  };

  return (
    <div className={style.auth_form_wrapper}>
      <h3 className={style.auth_form_head}>Авторизация</h3>
      <TextInput placeholder="Введите имя" handler={setName} />
      <TextInput placeholder="Введите логин" handler={setLogin} />

      <input
        type="password"
        className={style.auth_form_input}
        onChange={passwordInputHadler}
        placeholder="Введите пароль"
      />
      <button type="button" className={style.auth_form_btn} onClick={onSubmit}>
        Войти
      </button>
    </div>
  );
};

export default AuthForm;
