import React, { FC } from "react";
import Header from "../MainPage/Header";
import RegForm from "./RegForm";
import Footer from "../MainPage/Footer";
import style from "./RegPage.module.scss";

const AuthPage: FC = () => {
  return (
    <div className={style.reg_page_wrapper}>
      <Header />
      <RegForm />
      <Footer />
    </div>
  );
};

export default AuthPage;
