import React, { FC, useState } from "react";
import TextInput from "../../Common/Forms/TextInput";
import style from "./RegForm.module.scss";

const RegForm: FC = () => {
  const [login, setLogin] = useState<string>("");
  const [name, setName] = useState<string>("");
  const [phone, setPhone] = useState<string>("");
  const [password, setPassword] = useState<string>("");

  const passwordInputHadler = (event: any) => {
    setPassword(event.currentTarget.value);
  };

  return (
    <div className={style.reg_form_wrapper}>
      <h3 className={style.reg_form_head}>Регистрация</h3>
      <TextInput placeholder="Введите имя" handler={setName} />
      <TextInput placeholder="Придумайте логин" handler={setLogin} />
      <TextInput placeholder="Ваш номер телефона" handler={setPhone} />
      <input
        type="password"
        className={style.reg_form_input}
        onChange={passwordInputHadler}
        placeholder="Придумайте пароль"
      />
      <button
        type="button"
        className={style.reg_form_btn}
        onClick={() => console.log({ login, password, name, phone })}
      >
        Войти
      </button>
    </div>
  );
};
export default RegForm;
