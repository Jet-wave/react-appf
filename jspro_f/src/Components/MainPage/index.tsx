import React, { FC } from "react";
import Footer from "./Footer";
import Header from "./Header";
import MainInform from "./MainInform";
import style from "./MainPage.module.scss";

const MainPage: FC = () => {
  return (
    <div className={style.main_page_wrapper}>
      <Header />;
      <MainInform />
      <Footer />
    </div>
  );
};

export default MainPage;
