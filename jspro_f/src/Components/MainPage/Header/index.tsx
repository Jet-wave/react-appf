import React, { FC } from "react";
import Logo from "./Logo";
import NavMenu from "./NavMenu";
import NavButtons from "./NavButtons";
import style from "./Header.module.scss";

const Header: FC = () => {
  return (
    <header className={style.header_wrapper}>
      <Logo />

      <NavMenu />

      <NavButtons />
    </header>
  );
};

export default Header;
