import React, { FC } from "react";
import { Link } from "react-router-dom";
import style from "./NavButtons.module.scss";

const NavButtons: FC = () => {
  return (
    <>
      <div className={style["nav-buttons_wrapper"]}>
        <button className={style.button_wrapper} type="button">
          <Link to="/" className={style.link}>
            Главная
          </Link>
        </button>

        <button className={style.button_wrapper} type="button">
          <Link to="/auth" className={style.link}>
            Авторизация
          </Link>
        </button>

        <button className={style.button_wrapper} type="button">
          <Link to="/reg" className={style.link}>
            Регистрация
          </Link>
        </button>
      </div>
    </>
  );
};

export default NavButtons;
