import React, { FC } from "react";
import logo from "../../../../assets/logo_one.jpg";
import style from "./Logo.module.scss";

const Logo: FC = () => {
  return (
    <>
      <div className={style.logo_wrapper}>
        <img src={logo} alt="logo" />
      </div>
    </>
  );
};

export default Logo;
