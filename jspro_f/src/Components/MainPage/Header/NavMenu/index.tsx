import React, { FC, Profiler } from "react";
import { Link } from "react-router-dom";
import logo_one from "../../../../assets/city_logo/logowhiteran.png";
import logo_two from "../../../../assets/city_logo/logodanstar.png";
import logo_three from "../../../../assets/city_logo/logofolckrit.png";
import logo_four from "../../../../assets/city_logo/logomarcart.png";
import logo_five from "../../../../assets/city_logo/logomorfal.png";
import logo_six from "../../../../assets/city_logo/logoriften.png";
import logo_seven from "../../../../assets/city_logo/logosolitude.png";
import logo_eight from "../../../../assets/city_logo/logowindhelm.png";
import logo_nine from "../../../../assets/city_logo/logowinterhold.png";
import style from "./NavMenu.module.scss";

const NavMenu: FC = () => {
  return (
    <>
      <nav className={style.nav_wrapper}>
        <ul className={style.nav_list}>
          <Link to="/whiterun" className={style.link}>
            <li>
              <img src={logo_one} alt="logowhiterun" />
            </li>
          </Link>

          <Link to="dawnstar" className={style.link}>
          <li>
            <img src={logo_two} alt="logodawnstar" />
          </li>
          </Link>
          
          <Link to="folkrith" className={style.link}>
          <li>
            <img src={logo_three} alt="logofolkrith" />
          </li>
          </Link>

          <Link to="marcart" className={style.link}>
          <li>
            <img src={logo_four} alt="logowmarcart" />
          </li>
          </Link>

          <Link to="morfhal" className={style.link}>
          <li>
            <img src={logo_five} alt="logomorfhal" />
          </li>
          </Link>

          <Link to="riften" className={style.link}>
          <li>
            <img src={logo_six} alt="logoriften" />
          </li>
          </Link>

          <Link to="solitude" className={style.link}>
          <li>
            <img src={logo_seven} alt="logosolitude" />
          </li>
          </Link>

          <Link to="windhelm" className={style.link}>
          <li>
            <img src={logo_eight} alt="logowindhelm" />
          </li>
          </Link>

          <Link to="winterhold" className={style.link}>
          <li>
            <img src={logo_nine} alt="logowhiterun" />
          </li>
          </Link>
        </ul>

      </nav>
    </>
  );
};

export default NavMenu;
