import React, { FC } from "react";
import style from "../../Common/style_pages.module.scss";
import r from "../../../../assets/city_photo/r.png";
import riftenphoto from "../../../../assets/city_photo/riftenphoto.png";

const PageRiftenBox: FC = () => {
  
  return (

  <div className={style.dawnstar_box_wrapper} >
              <h1>Рифтен</h1>
              <div className={style.herb} >
              <img src={r}  alt="logomorfhal" />
              </div>
              <div className={style.photo} >
              <img className={style.img} src={riftenphoto} width="500" height="280" alt="logowhiterun" />
              </div>
              <div>
                <p>Рифтен (именуемый в ранних записях Рифтон) — город, находящийся на юго-востоке Скайрима близ границ 
                  Морровинда и Сиродила, на восточной оконечности озера Хонрик. Город является столицей владения Рифт, 
                  одного из «Четырёх Древних Владений». Один из 9 крупных городов. Ярл города — Лайла Рука Закона.</p>
                  
                  <p>Рифтен расположен в живописном краю Скайрима — в Осеннем лесу. Название в полной мере соответствует истине. 
                    Красота вторглась в город чудесной осенней листвой и приятной погодой. Однако большинство строений города деревянные,
                     старые на вид, вызывающие чувство жалости, которая часто охватывает гостей Рифтена. Нельзя сказать, что это сильно 
                     беспокоит жителей Рифтена, которые в действительности видят шумный, энергичный город с сильной экономикой, 
                     подпитываемой трудолюбивыми рыбаками и медоварами.</p>
                  
                  <p>Территория города со стороны берега ограничена крепостными стенами. Рифтен является единственным городом в Скайриме, 
                    имеющим районы, расположенные за пределами его стен.</p>
                    
                    <p>Город делится на 2 района петлёй большого канала, в последнее время пришедшего в упадок из-за отсутствия торговли во 
                      время гражданской войны и являющегося транспортной артерией для небольших грузовых судов. Высокие стены канала сложены из камня. 
                      На верхнем уровне канала расположены мосты, перекинутые с одного берега на другой, и деревянная набережная в виде нависающего над пристанями 
                      балкона с ограждениями. Благодаря высоте стен малые суда без проблем проходят под мостами к деревянным пристаням на нижнем уровне, расположенным 
                      на сваях почти на уровне воды. Крепкие двустворчатые ворота запирают оба выхода канала из Рифтена в озеро.</p>
                     </div>
            
  
  </div>
  );
};

export default PageRiftenBox;