import React, { FC } from "react";
import Header from "../../MainPage/Header";
import style from "../Common/style_pages_one.module.scss";
import PageRiftenBox from "./PageRiftenBox";

const PageRiften: FC = () => {
  return (
    <div className={style.d_page_wrapper}>
      <Header />;
      <PageRiftenBox />;
    </div>
  );
};

export default PageRiften;
