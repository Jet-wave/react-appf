import React, { FC } from "react";
import Header from "../../MainPage/Header";
import style from "../Common/style_pages_one.module.scss";
import PageMarcartBox from "./PageMarcartBox";

const PageMarcart: FC = () => {
  return (
    <div className={style.d_page_wrapper}>
      <Header />;
      <PageMarcartBox />;
      
    </div>
  );
};

export default PageMarcart;
