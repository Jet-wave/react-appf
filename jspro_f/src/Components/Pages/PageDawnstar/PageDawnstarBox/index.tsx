import React, { FC, useState } from "react";
import style from "../../Common/style_pages.module.scss";
import d from "../../../../assets/city_photo/d.png";
import dawnstarphoto from "../../../../assets/city_photo/dawnstarphoto.png";

const PageDawnstarBox: FC = () => {
  
  return (

  <div className={style.dawnstar_box_wrapper} >
              <h1>Данстар</h1>
              <div className={style.herb} >
              <img src={d}  alt="logowhiterun" />
              </div>
              <div className={style.photo} >
              <img className={style.img} src={dawnstarphoto} width="500" height="280" alt="logowhiterun" />
              </div>
              <div>
                <p>Данстар — портовый город, располагающийся на северном побережье Скайрима, 
                  столица владения Белый Берег. Правитель здесь — ярл Скальд. Один из самых 
                  маленьких городов в Скайриме, но очень уютный, является домом для нескольких 
                  семей рыбаков и шахтёров.</p>
                  <p>Неподалёку от города расположен лагерь Имперского легиона, 
                    появившийся в связи с активизацией деятельности повстанцев в этом районе.
                     Первоначально город находится под контролем Братьев Бури.</p>
                     </div>

                    
  
  </div>
  );
};

export default PageDawnstarBox;