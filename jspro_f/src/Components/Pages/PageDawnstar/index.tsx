import React, { FC } from "react";
import Header from "../../MainPage/Header";
import style from "../Common/style_pages_one.module.scss";
import PageDawnstarBox from "./PageDawnstarBox";

const PageDawnstar: FC = () => {
  return (
    <div className={style.d_page_wrapper}>
      <Header />;
      <PageDawnstarBox />;
      
    </div>
  );
};

export default PageDawnstar;
