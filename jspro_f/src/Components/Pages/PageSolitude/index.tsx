import React, { FC } from "react";
import Header from "../../MainPage/Header";
import style from "../Common/style_pages_one.module.scss";
import PageSolitudeBox from "./PageSolitudeBox";

const PageSolitude: FC = () => {
  return (
    <div className={style.d_page_wrapper}>
      <Header />;
      <PageSolitudeBox />;
    </div>
  );
};

export default PageSolitude;
