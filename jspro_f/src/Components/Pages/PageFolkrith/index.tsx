import React, { FC } from "react";
import Header from "../../MainPage/Header";
import style from "../Common/style_pages_one.module.scss";
import PageFolkrithBox from "./PageFolkrithBox";

const PageFolkrith: FC = () => {
  return (
    <div className={style.d_page_wrapper}>
      <Header />;
      <PageFolkrithBox />;
    </div>
  );
};

export default PageFolkrith;
