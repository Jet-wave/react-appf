import React, { FC } from "react";
import Header from "../../MainPage/Header";
import style from "../Common/style_pages_one.module.scss";
import PageMorfhalBox from "./PageMorhalBox";

const PageMorfhal: FC = () => {
  return (
    <div className={style.d_page_wrapper}>
      <Header />;
      <PageMorfhalBox />;
      
    </div>
  );
};

export default PageMorfhal;
