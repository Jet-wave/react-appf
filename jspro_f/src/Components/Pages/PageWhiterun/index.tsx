import React, { FC } from "react";
import Header from "../../MainPage/Header";
import style from "../Common/style_pages_one.module.scss";
import PageWhiterunBox from "./PageWhiterunBox";

const PageWhiterun: FC = () => {
  return (
    <div className={style.d_page_wrapper}>
      <Header />;
      <PageWhiterunBox />;
    </div>
  );
};

export default PageWhiterun;
