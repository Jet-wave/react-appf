import React, { FC } from "react";
import Header from "../../MainPage/Header";
import style from "../Common/style_pages_one.module.scss";
import PageWindhelmBox from "./PageWindhelmBox";

const PageWindhelm: FC = () => {
  return (
    <div className={style.d_page_wrapper}>
      <Header />;
      <PageWindhelmBox />;
      
    </div>
  );
};

export default PageWindhelm;
