import React, { FC } from "react";
import Header from "../../MainPage/Header";
import style from "../Common/style_pages_one.module.scss";
import PageWinterholdBox from "./PageWinterholdBox";

const PageWinterhold: FC = () => {
  return (
    <div className={style.d_page_wrapper}>
      <Header />;
      <PageWinterholdBox />;
      
    </div>
  );
};

export default PageWinterhold;
