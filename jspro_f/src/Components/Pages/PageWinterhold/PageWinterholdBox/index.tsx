import React, { FC } from "react";
import style from "../../Common/style_pages.module.scss";
import wint from "../../../../assets/city_photo/wint.png";
import winterholdphoto from "../../../../assets/city_photo/winterholdphoto.png";

const PageWinterholdBox: FC = () => {
  
  return (

  <div className={style.dawnstar_box_wrapper} >
              <h1>Винтерхолд</h1>
              <div className={style.herb} >
              <img src={wint}  alt="logowinterhold" />
              </div>
              <div className={style.photo} >
              <img className={style.img} src={winterholdphoto} width="500" height="280" alt="logowinterhold" />
              </div>
              <div>
                <p>Винтерхолд — город с насыщенной и сложной историей. В далёком прошлом был первой столицей Скайрима, что делало его важным торговым и культурным центром. Именно в те золотые для города годы возле него была основана Коллегия Винтерхолда.</p>
                <p>Судьбоносным (и наиболее ужасным) событием для города стал Великий обвал, произошедший в 4Э 122. Чудовищный шторм, бушевавший несколько дней подряд и сопровождавшийся цунами, вызвал обвал большого участка берегового скального массива вместе с основной частью располагавшегося на нём Винтерхолда. После этой катастрофы от некогда великого города осталось лишь несколько зданий, расположенных вдоль единственной уцелевшей улицы.</p>
                <p>Совершенно загадочным образом здание Коллегии практически не пострадало. Это вызвало закономерные подозрения о причинах обвала. Сами маги всячески отрицали свою причастность к трагедии, в итоге их вина так и не была доказана. Однако отголоски возникшего недоверия к магии проявляются и по сей день.</p>
                <p>Географически Винтерхолд расположен на северо-восточном побережье Скайрима, в пустынной и неприветливой местности, удаленной от основных торговых маршрутов. От окончательного упадка и забвения его, по иронии судьбы, спасает лишь Коллегия, предоставляющая магические услуги желающим и обучение начинающим магам, но даже это не изменяет отрицательного к ней отношения со стороны большинства жителей городка.</p>
                <p>Нынешним правителем города является ярл Корир.</p>
                     </div>

                    
  
  </div>
  );
};

export default PageWinterholdBox;
